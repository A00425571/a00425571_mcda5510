/**
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.csv.*;

/**
 * @author lourdesroashan
 *
 */
public class AnalyzeCSV {

	public static int invalid = 0;
	public static int valid = 0;
	
	
	public void parseCsv(String fileName, String path, PrintWriter pw) {
		
		String fileExt = fileName.substring(fileName.length() - 3);
		String accFile = "csv";
		
		if(fileExt.equals(accFile)) {
	        	Reader in;
	    		try {
	    			in = new FileReader(path);	    			
	    			Iterable<CSVRecord> records = CSVFormat.EXCEL.withIgnoreEmptyLines(false).parse(in);	    			
	    			int i = 0;
	    			for (CSVRecord record : records) {
	
		    			if(i==0) {
		    				
		    			}
		    			
		    			else {
		    			    String fName = record.get(0);
		    			    String lName = record.get(1);
		    			    String stNo = record.get(2);
		    			    String street = record.get(3);
		    			    String city = record.get(4);
		    			    String province = record.get(5);
		    			    String country = record.get(6);
		    			    String postalCode = record.get(7);
		    			    String phoneNo = record.get(8);
		    			    String emailId = record.get(9);
			    			
		    			    if(		fName.isEmpty() || 
					    			lName.isEmpty() || 
					    			stNo.isEmpty() || 
					    			street.isEmpty() || 
					    			city.isEmpty() ||
					    			province.isEmpty() || 
					    			country.isEmpty() || 
					    			postalCode.isEmpty() || 
					    			phoneNo.isEmpty() || 
					    			emailId.isEmpty()) {		
		    			    	
					    			invalid = invalid + 1;
					    		}
			    			else {
			    				//write into file here
			    			    valid = valid + 1;
			    			    StringBuilder builder = new StringBuilder();
			    			    builder.append(fName+",");
					    		builder.append(lName+",");
					    		builder.append(stNo+",");
					    		builder.append(street+",");
					    		builder.append(city+",");
					    		builder.append(province+",");
					    		builder.append(country+",");
					    		builder.append(postalCode+",");
					    		builder.append(phoneNo+",");
					    		builder.append(emailId);
					    		builder.append('\n');
			    			    pw.write(builder.toString());			    			    
			    			}		    			    
		    			}
		    			i = i + 1;
	    			}			
	    			
	    		} catch ( IllegalArgumentException e) {
	    			e.printStackTrace();
	    		} catch (IOException e) {					
					e.printStackTrace();
				}    		
		}
		
	}
	
	public void walk( String path, PrintWriter pw ) {
		
        File root = new File( path );
        File[] list = root.listFiles();
        
        if (list == null) return;
        
        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() , pw);                
            }
            else {                	
            		String fileName = f.getAbsoluteFile().getName();            		
            		parseCsv(fileName, f.getAbsolutePath(), pw);       	
            	
            }
        }
    }
	
	
	
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		final long startTime = System.currentTimeMillis();
		AnalyzeCSV fw = new AnalyzeCSV();

		PrintWriter pw = new PrintWriter(new File("newfile.csv"));
	    StringBuilder builder = new StringBuilder();
	    String ColumnNamesList = "firstname,lastname,streetno,street,city,province,country,postalcode,phoneno, email";
	    builder.append(ColumnNamesList +"\n");
	    pw.write(builder.toString());
		
        fw.walk("data_files", pw);

        pw.close();
        final long endTime = System.currentTimeMillis();
        
        Logger logger = Logger.getLogger("mylogs");
        FileHandler fh;
        
        try {  

            // This block configure the logger with handler and formatter  
            fh = new FileHandler("MyLogFile.log");  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  

            // the following statement is used to log any messages  
            logger.info("Invalid Rows: "+invalid);
            logger.info("Valid Rows: "+valid);
            logger.info("Time: "+(endTime - startTime));

        } catch (SecurityException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
      
        System.out.println("Total execution time: " + (endTime - startTime) +" ms");
        System.out.println("Invalid : "+invalid);
        System.out.println("Valid : "+ valid);

		

	}

}
