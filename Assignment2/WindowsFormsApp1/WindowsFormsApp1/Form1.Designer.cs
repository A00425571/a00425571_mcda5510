﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.idLabel = new System.Windows.Forms.Label();
            this.fnameLabel = new System.Windows.Forms.Label();
            this.lnameLabel = new System.Windows.Forms.Label();
            this.stLabel = new System.Windows.Forms.Label();
            this.cityLabel = new System.Windows.Forms.Label();
            this.provLabel = new System.Windows.Forms.Label();
            this.cntLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.phnoLabel = new System.Windows.Forms.Label();
            this.pcodeLabel = new System.Windows.Forms.Label();
            this.pcodeText = new System.Windows.Forms.TextBox();
            this.cntText = new System.Windows.Forms.TextBox();
            this.provText = new System.Windows.Forms.TextBox();
            this.cityText = new System.Windows.Forms.TextBox();
            this.stnoText = new System.Windows.Forms.TextBox();
            this.lnameText = new System.Windows.Forms.TextBox();
            this.fnameText = new System.Windows.Forms.TextBox();
            this.idTextbox = new System.Windows.Forms.TextBox();
            this.phnoText = new System.Windows.Forms.TextBox();
            this.emailText = new System.Windows.Forms.TextBox();
            this.nxtBtn = new System.Windows.Forms.Button();
            this.prevBtn = new System.Windows.Forms.Button();
            this.browseBtn = new System.Windows.Forms.Button();
            this.importBtn = new System.Windows.Forms.Button();
            this.pathText = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.addrLabel = new System.Windows.Forms.Label();
            this.addrText = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.errorStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(140, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "CONTACT FORM";
            // 
            // idLabel
            // 
            this.idLabel.AutoSize = true;
            this.idLabel.Location = new System.Drawing.Point(41, 75);
            this.idLabel.Name = "idLabel";
            this.idLabel.Size = new System.Drawing.Size(41, 13);
            this.idLabel.TabIndex = 1;
            this.idLabel.Text = "ID (PK)";
            // 
            // fnameLabel
            // 
            this.fnameLabel.AutoSize = true;
            this.fnameLabel.Location = new System.Drawing.Point(41, 105);
            this.fnameLabel.Name = "fnameLabel";
            this.fnameLabel.Size = new System.Drawing.Size(57, 13);
            this.fnameLabel.TabIndex = 2;
            this.fnameLabel.Text = "First Name";
            // 
            // lnameLabel
            // 
            this.lnameLabel.AutoSize = true;
            this.lnameLabel.Location = new System.Drawing.Point(41, 136);
            this.lnameLabel.Name = "lnameLabel";
            this.lnameLabel.Size = new System.Drawing.Size(58, 13);
            this.lnameLabel.TabIndex = 3;
            this.lnameLabel.Text = "Last Name";
            // 
            // stLabel
            // 
            this.stLabel.AutoSize = true;
            this.stLabel.Location = new System.Drawing.Point(41, 165);
            this.stLabel.Name = "stLabel";
            this.stLabel.Size = new System.Drawing.Size(55, 13);
            this.stLabel.TabIndex = 4;
            this.stLabel.Text = "Street No.";
            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Location = new System.Drawing.Point(41, 195);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(24, 13);
            this.cityLabel.TabIndex = 5;
            this.cityLabel.Text = "City";
            // 
            // provLabel
            // 
            this.provLabel.AutoSize = true;
            this.provLabel.Location = new System.Drawing.Point(41, 227);
            this.provLabel.Name = "provLabel";
            this.provLabel.Size = new System.Drawing.Size(49, 13);
            this.provLabel.TabIndex = 6;
            this.provLabel.Text = "Province";
            // 
            // cntLabel
            // 
            this.cntLabel.AutoSize = true;
            this.cntLabel.Location = new System.Drawing.Point(41, 296);
            this.cntLabel.Name = "cntLabel";
            this.cntLabel.Size = new System.Drawing.Size(43, 13);
            this.cntLabel.TabIndex = 7;
            this.cntLabel.Text = "Country";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(43, 399);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(46, 13);
            this.emailLabel.TabIndex = 8;
            this.emailLabel.Text = "Email ID";
            // 
            // phnoLabel
            // 
            this.phnoLabel.AutoSize = true;
            this.phnoLabel.Location = new System.Drawing.Point(41, 362);
            this.phnoLabel.Name = "phnoLabel";
            this.phnoLabel.Size = new System.Drawing.Size(58, 13);
            this.phnoLabel.TabIndex = 9;
            this.phnoLabel.Text = "Phone No.";
            // 
            // pcodeLabel
            // 
            this.pcodeLabel.AutoSize = true;
            this.pcodeLabel.Location = new System.Drawing.Point(41, 329);
            this.pcodeLabel.Name = "pcodeLabel";
            this.pcodeLabel.Size = new System.Drawing.Size(64, 13);
            this.pcodeLabel.TabIndex = 10;
            this.pcodeLabel.Text = "Postal Code";
            // 
            // pcodeText
            // 
            this.pcodeText.Location = new System.Drawing.Point(155, 322);
            this.pcodeText.Name = "pcodeText";
            this.pcodeText.Size = new System.Drawing.Size(137, 20);
            this.pcodeText.TabIndex = 11;
            // 
            // cntText
            // 
            this.cntText.Location = new System.Drawing.Point(155, 289);
            this.cntText.Name = "cntText";
            this.cntText.Size = new System.Drawing.Size(137, 20);
            this.cntText.TabIndex = 12;
            // 
            // provText
            // 
            this.provText.Location = new System.Drawing.Point(155, 220);
            this.provText.Name = "provText";
            this.provText.Size = new System.Drawing.Size(137, 20);
            this.provText.TabIndex = 13;
            // 
            // cityText
            // 
            this.cityText.Location = new System.Drawing.Point(155, 192);
            this.cityText.Name = "cityText";
            this.cityText.Size = new System.Drawing.Size(137, 20);
            this.cityText.TabIndex = 14;
            // 
            // stnoText
            // 
            this.stnoText.Location = new System.Drawing.Point(155, 162);
            this.stnoText.Name = "stnoText";
            this.stnoText.Size = new System.Drawing.Size(137, 20);
            this.stnoText.TabIndex = 15;
            // 
            // lnameText
            // 
            this.lnameText.Location = new System.Drawing.Point(155, 136);
            this.lnameText.Name = "lnameText";
            this.lnameText.Size = new System.Drawing.Size(137, 20);
            this.lnameText.TabIndex = 16;
            // 
            // fnameText
            // 
            this.fnameText.Location = new System.Drawing.Point(155, 105);
            this.fnameText.Name = "fnameText";
            this.fnameText.Size = new System.Drawing.Size(137, 20);
            this.fnameText.TabIndex = 17;
            // 
            // idTextbox
            // 
            this.idTextbox.Location = new System.Drawing.Point(155, 75);
            this.idTextbox.Name = "idTextbox";
            this.idTextbox.ReadOnly = true;
            this.idTextbox.Size = new System.Drawing.Size(137, 20);
            this.idTextbox.TabIndex = 18;
            // 
            // phnoText
            // 
            this.phnoText.Location = new System.Drawing.Point(155, 359);
            this.phnoText.Name = "phnoText";
            this.phnoText.Size = new System.Drawing.Size(137, 20);
            this.phnoText.TabIndex = 19;
            // 
            // emailText
            // 
            this.emailText.Location = new System.Drawing.Point(155, 392);
            this.emailText.Name = "emailText";
            this.emailText.Size = new System.Drawing.Size(257, 20);
            this.emailText.TabIndex = 20;
            // 
            // nxtBtn
            // 
            this.nxtBtn.Location = new System.Drawing.Point(180, 434);
            this.nxtBtn.Name = "nxtBtn";
            this.nxtBtn.Size = new System.Drawing.Size(75, 23);
            this.nxtBtn.TabIndex = 21;
            this.nxtBtn.Text = "Next";
            this.nxtBtn.UseVisualStyleBackColor = true;
            this.nxtBtn.Click += new System.EventHandler(this.nxtBtn_Click);
            // 
            // prevBtn
            // 
            this.prevBtn.Location = new System.Drawing.Point(44, 434);
            this.prevBtn.Name = "prevBtn";
            this.prevBtn.Size = new System.Drawing.Size(75, 23);
            this.prevBtn.TabIndex = 22;
            this.prevBtn.Text = "Previous";
            this.prevBtn.UseVisualStyleBackColor = true;
            this.prevBtn.Click += new System.EventHandler(this.prevBtn_Click);
            // 
            // browseBtn
            // 
            this.browseBtn.Location = new System.Drawing.Point(256, 550);
            this.browseBtn.Name = "browseBtn";
            this.browseBtn.Size = new System.Drawing.Size(75, 23);
            this.browseBtn.TabIndex = 23;
            this.browseBtn.Text = "Browse";
            this.browseBtn.UseVisualStyleBackColor = true;
            this.browseBtn.Click += new System.EventHandler(this.browseBtn_Click);
            // 
            // importBtn
            // 
            this.importBtn.Location = new System.Drawing.Point(337, 550);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(75, 23);
            this.importBtn.TabIndex = 24;
            this.importBtn.Text = "Import";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // pathText
            // 
            this.pathText.Location = new System.Drawing.Point(44, 524);
            this.pathText.Name = "pathText";
            this.pathText.Size = new System.Drawing.Size(368, 20);
            this.pathText.TabIndex = 25;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "CSV Files Only|*.csv";
            // 
            // addrLabel
            // 
            this.addrLabel.AutoSize = true;
            this.addrLabel.Location = new System.Drawing.Point(41, 261);
            this.addrLabel.Name = "addrLabel";
            this.addrLabel.Size = new System.Drawing.Size(45, 13);
            this.addrLabel.TabIndex = 26;
            this.addrLabel.Text = "Address";
            // 
            // addrText
            // 
            this.addrText.Location = new System.Drawing.Point(155, 254);
            this.addrText.Name = "addrText";
            this.addrText.Size = new System.Drawing.Size(137, 20);
            this.addrText.TabIndex = 27;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.errorStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 575);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(424, 22);
            this.statusStrip1.TabIndex = 28;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // errorStatus
            // 
            this.errorStatus.Name = "errorStatus";
            this.errorStatus.Size = new System.Drawing.Size(118, 17);
            this.errorStatus.Text = "toolStripStatusLabel1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 597);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.addrText);
            this.Controls.Add(this.addrLabel);
            this.Controls.Add(this.pathText);
            this.Controls.Add(this.importBtn);
            this.Controls.Add(this.browseBtn);
            this.Controls.Add(this.prevBtn);
            this.Controls.Add(this.nxtBtn);
            this.Controls.Add(this.emailText);
            this.Controls.Add(this.phnoText);
            this.Controls.Add(this.idTextbox);
            this.Controls.Add(this.fnameText);
            this.Controls.Add(this.lnameText);
            this.Controls.Add(this.stnoText);
            this.Controls.Add(this.cityText);
            this.Controls.Add(this.provText);
            this.Controls.Add(this.cntText);
            this.Controls.Add(this.pcodeText);
            this.Controls.Add(this.pcodeLabel);
            this.Controls.Add(this.phnoLabel);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.cntLabel);
            this.Controls.Add(this.provLabel);
            this.Controls.Add(this.cityLabel);
            this.Controls.Add(this.stLabel);
            this.Controls.Add(this.lnameLabel);
            this.Controls.Add(this.fnameLabel);
            this.Controls.Add(this.idLabel);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Windows Contact Form";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label idLabel;
        private System.Windows.Forms.Label fnameLabel;
        private System.Windows.Forms.Label lnameLabel;
        private System.Windows.Forms.Label stLabel;
        private System.Windows.Forms.Label cityLabel;
        private System.Windows.Forms.Label provLabel;
        private System.Windows.Forms.Label cntLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Label phnoLabel;
        private System.Windows.Forms.Label pcodeLabel;
        private System.Windows.Forms.TextBox pcodeText;
        private System.Windows.Forms.TextBox cntText;
        private System.Windows.Forms.TextBox provText;
        private System.Windows.Forms.TextBox cityText;
        private System.Windows.Forms.TextBox stnoText;
        private System.Windows.Forms.TextBox lnameText;
        private System.Windows.Forms.TextBox fnameText;
        private System.Windows.Forms.TextBox idTextbox;
        private System.Windows.Forms.TextBox phnoText;
        private System.Windows.Forms.TextBox emailText;
        private System.Windows.Forms.Button nxtBtn;
        private System.Windows.Forms.Button prevBtn;
        private System.Windows.Forms.Button browseBtn;
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.TextBox pathText;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label addrLabel;
        private System.Windows.Forms.TextBox addrText;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel errorStatus;
    }
}

