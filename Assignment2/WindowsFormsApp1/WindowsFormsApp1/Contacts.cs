﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace WindowsFormsApp1
{
     public class Contacts
    {
        public string id;
        public string FirstName;
        public string LastName;
        public string StreetNo;
        public string address;
        public string city;
        public string province;
        public string country;
        public string postalCode;
        public string phoneNo;
        public string emailId;

        //SQL Connection String
        string conStr = "Data Source=DESKTOP-2BS170N;Initial Catalog=testdb;Integrated Security=True";
        Random r = new Random();
        bool check;


        //Method to clear the contents in a table
        public void clearData()
        {
            SqlConnection myConnection = new SqlConnection(conStr);
            myConnection.Open();
            try
            {
               
                string sqlQuery = "TRUNCATE TABLE [testdb].[dbo].[contact]";
                SqlCommand myCommand = new SqlCommand(sqlQuery,
                                                         myConnection);
                myCommand.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
      
            myConnection.Close();

        }


        //Method to select the data from the table and add it to the entity class
        public List<Contacts> GetData()
        {
            List<Contacts> resContact = new List<Contacts>();
            SqlConnection myConnection = new SqlConnection(conStr);
            try
            {
                myConnection.Open();

                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("select * from [testdb].[dbo].[contact]",
                                                         myConnection);
                myReader = myCommand.ExecuteReader();

                while (myReader.Read())
                {
                    Contacts c = new Contacts();
                    c.id = myReader["id"].ToString();
                    c.FirstName = myReader["fname"].ToString();
                    c.LastName = myReader["lname"].ToString();
                    c.StreetNo = myReader["streetno"].ToString();
                    c.address = myReader["address"].ToString();
                    c.city = myReader["city"].ToString();
                    c.province = myReader["province"].ToString();
                    c.country = myReader["country"].ToString();
                    c.postalCode = myReader["postalcode"].ToString();
                    c.phoneNo = myReader["phoneno"].ToString();
                    c.emailId = myReader["email"].ToString();

                    resContact.Add(c);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            myConnection.Close();
            return resContact;
        }


        //Method to insert data into the table
        public bool Save(List<Contacts> contactList)
        {           
            SqlConnection myConnection = new SqlConnection(conStr);
                    
            try
            {
                myConnection.Open();
                foreach (Contacts contact in contactList)
                {
                    
                    String randomAddress = contact.StreetNo + ", " + randomAddressGenerator();
                    string insertStr = "INSERT INTO [testdb].[dbo].[contact] (fname, lname, streetno, address, city, province, country, postalcode, phoneno, email)" +
                                       "Values ('" + contact.FirstName + 
                                       "','"
                                       +contact.LastName+
                                       "','"
                                       +contact.StreetNo+
                                       "','"
                                       + randomAddress +
                                       "','"
                                       + contact.city +
                                       "','"
                                       + contact.province +
                                       "','"
                                       + contact.country +
                                       "','"
                                       + contact.postalCode +
                                       "','"
                                       + contact.phoneNo +
                                       "','"
                                       + contact.emailId +
                                       "')";


                    SqlCommand myCommand = new SqlCommand(insertStr, myConnection);

                    int rows = myCommand.ExecuteNonQuery();
                    Console.WriteLine(rows);
                    if (rows == 0)
                    {
                        check = false;
                        break;
                    }
                    else
                    {
                        check = true;
                    }
                    
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                check = false;
            }

            myConnection.Close();
            return check;
        }


        //Returns a randomly generated street address
        public string randomAddressGenerator()
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            int length = r.Next(10, 15);
            string addr = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[r.Next(s.Length)]).ToArray());

            string fullAddr = addr + " Street";
            return fullAddr;
        }









    }




}
