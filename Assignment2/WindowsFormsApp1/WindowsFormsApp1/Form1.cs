﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        List<Contacts> contact = new List<Contacts>();
        List<Contacts> readContact = new List<Contacts>();
        Contacts currentPerson = new Contacts();
        int index;


        public Form1()
        {
            InitializeComponent();
            errorStatus.Text = "";
        }

        private void browseBtn_Click(object sender, EventArgs e)
        {
            //open the file dialog when browse button is clicked
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {

                pathText.Text = openFileDialog.FileName;
                Console.WriteLine(pathText.Text);
            }
            else
            {
                pathText.Text = "";
            }

        }

        private void importBtn_Click(object sender, EventArgs e)
        {

            if (pathText.Text == "")
            {
                MessageBox.Show("No CSV File Selected");
            }
            else
            {
                //start reading the file and split it using the comma delimiter
                using (var reader = new StreamReader(pathText.Text))
                {
                    int i = 0; //a counter just to skip reading the headers
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (i > 0)
                        {

                            var values = line.Split(',');

                            Contacts c = new Contacts();
                            c.FirstName = values[0];
                            c.LastName = values[1];
                            c.StreetNo = values[2];
                            c.city = values[3];
                            c.province = values[4];
                            c.country = values[5];
                            c.postalCode = values[6];
                            c.phoneNo = values[7];
                            c.emailId = values[8];
                            contact.Add(c);
                        }
                        i = i + 1;
                    }
                }
                pathText.Text = "";
                Console.WriteLine(contact[0].FirstName);
                Contacts con = new Contacts();
                bool r = con.Save(contact);

                if (r == true)
                {
                    MessageBox.Show("Contacts Imported");
                    readContact = con.GetData();
                    currentPerson = readContact[0];
                    index = readContact.IndexOf(currentPerson);
                    setData(readContact, index);
                }
                else if(r == false)
                {
                    MessageBox.Show("Import Failed");
                    contact.Clear();
                    //truncating the table if it fails just in case
                }

            }
        }

        private void nxtBtn_Click(object sender, EventArgs e)
        {

            index = index + 1;
            if (index < readContact.Count && index >= 0)
            {
                setData(readContact, index);
                nxtBtn.Enabled = true;
            }
            //Logic to Disable and Enable Next and Previous Button
            if (index == readContact.Count)
            {
                nxtBtn.Enabled = false;

            }
            if (index > 0)
            {
                prevBtn.Enabled = true;
            }


        }

        private void prevBtn_Click(object sender, EventArgs e)
        {

            index = index - 1;
            if (index >= 0)
            {
                setData(readContact, index);

            }
            //logic to disable to Next and Previous Button
            if (index <= 0)
            {
                prevBtn.Enabled = false;
            }
            if (index < readContact.Count)
            {
                nxtBtn.Enabled = true;
            }

        }


        //call this method to display the data for each contact
        public void setData(List<Contacts> contactList, int index)
        {

            idTextbox.Text = contactList[index].id;
            fnameText.Text = contactList[index].FirstName;
            lnameText.Text = contactList[index].LastName;
            stnoText.Text = contactList[index].StreetNo;
            addrText.Text = readContact[index].address;
            cityText.Text = contactList[index].city;
            provText.Text = contactList[index].province;
            cntText.Text = contactList[index].country;
            pcodeText.Text = contactList[index].postalCode;
            phnoText.Text = contactList[index].phoneNo;
            emailText.Text = contactList[index].emailId;

            //call validation for every check
            validateData(contactList[index]);

        }

        public void validateData(Contacts cList)
        {
             
            if(cList.FirstName.Length > 50)
            {
                errorStatus.Text = "First Name Exceeds 50 Characters";
            }
            else if (cList.LastName.Length > 50)
            {
                errorStatus.Text = "Last Name Exceeds 50 Characters";
            }
            else if (cList.address.Length > 50)
            {
                errorStatus.Text = "Address Exceeds 50 Characters";
            }
            else if (cList.city.Length > 50)
            {
                errorStatus.Text = "City Exceeds 50 Characters";
            }
            else if (cList.province.Length > 50)
            {
                errorStatus.Text = "Province Exceeds 50 Characters";
            }
            else if(cList.postalCode.Length > 7)
            {
                errorStatus.Text = "Postal Code Exceeds 7 Characters";
            }
            else
            {
                errorStatus.Text = "All OK!";
            }


        }


    }
}
