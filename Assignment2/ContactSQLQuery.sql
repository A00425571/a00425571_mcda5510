USE [testdb]
GO

/****** Object:  Table [dbo].[contact]    Script Date: 2018-02-12 10:55:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[contact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fname] [nvarchar](100) NULL,
	[lname] [nvarchar](100) NULL,
	[streetno] [nvarchar](100) NULL,
	[address] [nvarchar](100) NULL,
	[city] [nvarchar](100) NULL,
	[province] [nvarchar](100) NULL,
	[country] [nvarchar](100) NULL,
	[postalcode] [nvarchar](100) NULL,
	[phoneno] [nvarchar](100) NULL,
	[email] [nvarchar](100) NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


