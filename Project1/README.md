# README for Project 1 #

Our project is called "SMUtter" - which is basically Twitter for SMU. Hence the name, 'SMUtter". It combines the minimal social networking features of twitter but exclusively for SMU students. Since twitter is a large social networking platform, we plan to make a similar platform for SMU students, making it easier for them to follow other students' activities and updates. We chose this as our project as it will help us learn most of the functionalities of making an ASP.NET web application end to end. I strongly feel that this project is a tad more complex that the given project as it includes several features which are not included in the given project. For example, we plan to do user login using 'HTTP Sessions', 'Writing complex LINQ queries to display data accordingly, using partial views, passing data from different controllers and so on. 




## Team Members ##
Vivek Nagamanickam - A00422230
Lourdes Roashan Vechoor Padmanabhan - A00425571

This is project 1.

