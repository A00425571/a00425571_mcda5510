﻿
$(document).ready(function () {
    $('#lenSweet').html(120);
    $('#enterSweet').on('input change', function () {
        var my_txt = $(this).val();
        var len = my_txt.length;
        $('#lenSweet').html(120 - len);
        if (len > 120) {
            $('#btnSweet').prop('disabled', 'true');
        }
        else if (len <= 120) {
            $('#btnSweet').removeAttr('disabled');
        }
    });
});