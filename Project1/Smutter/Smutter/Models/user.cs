namespace Smutter.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class user
    {
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string username { get; set; }

        [Required]
        [StringLength(50)]
        public string firstname { get; set; }

        [Required]
        [StringLength(50)]
        public string lastname { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Password must contain atleast 7 character", MinimumLength = 7)]
        public string password { get; set; }

        [Required]
        [StringLength(100)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@s(mu)\\.ca$", ErrorMessage ="Please enter a valid email address")]
        public string email { get; set; }
        //[RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter a valid email address")]
        [Column(TypeName = "text")]
        [Required(ErrorMessage = "Please enter something about you!")]
        public string bio { get; set; }

        [Column(TypeName = "date")]
        [Required(ErrorMessage = "Please enter your date of birth")]
        [dob(MinAge = 18, MaxAge = 110)]
        public DateTime? dob { get; set; }

        public class dobAttribute : ValidationAttribute
        {
            public int MinAge { get; set; }
            public int MaxAge { get; set; }

            public override bool IsValid(Object value)
            {
                if (value == null)
                    return true;
                    var val = (DateTime)value;

                if (val.AddYears(MinAge) > DateTime.Now)
                    return false;

                return (val.AddYears(MaxAge) > DateTime.Now);
            }
        }

    }
}
