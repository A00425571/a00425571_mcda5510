namespace Smutter.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FollowContext : DbContext
    {
        public FollowContext()
            : base("name=FollowContext")
        {
        }

        public virtual DbSet<follower> followers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
