namespace Smutter.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TweetContext : DbContext
    {
        public TweetContext()
            : base("name=TweetContext")
        {
        }

        public virtual DbSet<tweet> tweets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tweet>()
                .Property(e => e.tweet1)
                .IsUnicode(false);
        }
    }
}
