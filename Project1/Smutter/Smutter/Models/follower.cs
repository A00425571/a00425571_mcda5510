namespace Smutter.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class follower
    {
        [Key]
        public int fid { get; set; }

        public int follower_id { get; set; }

        public int followed_id { get; set; }
    }
}
