namespace Smutter.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tweet
    {
        [Key]
        public int tid { get; set; }

        public int uid { get; set; }

        [Column("tweet")]
        [Required]
        [StringLength(120)]
        public string tweet1 { get; set; }

        public DateTime timestamp { get; set; }

        [NotMapped]
        public string username { get; set; }
    }
}
