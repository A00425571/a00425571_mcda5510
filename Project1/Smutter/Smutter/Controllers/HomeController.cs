﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Smutter.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(Session["uid"] != null)
            {
                return RedirectToAction("UserProfile", "Tweets");
            }
            else
            {
                ViewBag.Title = "Home Page";

                return View();
            }

        }
    }
}
