﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Smutter.Models;

namespace Smutter.Controllers
{
    public class TweetsController : Controller
    {
        private TweetContext db = new TweetContext();

        // GET: Tweets
        public ActionResult Index()
        {
            return View(db.tweets.ToList());
        }

        // GET: Tweets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tweet tweet = db.tweets.Find(id);
            if (tweet == null)
            {
                return HttpNotFound();
            }
            return View(tweet);
        }

        // GET: Tweets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tweets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "tid,uid,tweet1,timestamp")] tweet tweet)
        {
            if (ModelState.IsValid)
            {
                db.tweets.Add(tweet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tweet);
        }

        // GET: Tweets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tweet tweet = db.tweets.Find(id);
            if (tweet == null)
            {
                return HttpNotFound();
            }
            return View(tweet);
        }

        // POST: Tweets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "tid,uid,tweet1,timestamp")] tweet tweet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tweet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tweet);
        }

        // GET: Tweets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tweet tweet = db.tweets.Find(id);
            if (tweet == null)
            {
                return HttpNotFound();
            }
            return View(tweet);
        }

        // POST: Tweets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tweet tweet = db.tweets.Find(id);
            db.tweets.Remove(tweet);
            db.SaveChanges();
            return RedirectToAction("UserProfile","Tweets");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult UserProfile()
        {

            if (Session["uid"] != null)
            {
                var twts = from myRow in db.tweets.AsEnumerable()
                           where myRow.uid == System.Convert.ToInt32(Session["uid"])
                           select myRow;
                return View(twts.ToList());
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }

            
        }


        [ChildActionOnly]
        public ActionResult EnterTweet()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnterTweet(tweet twt)
        {
            if (ModelState.IsValid)
            {
                if(twt.tweet1.Length > 120)
                {
                    ModelState.AddModelError("", "SWEETS should not contain more than 120 characters");
                }
                else { 
                    twt.uid = System.Convert.ToInt32(Session["uid"]);
                    var d = DateTime.Now;
                    twt.timestamp = d;
                    db.tweets.Add(twt);
                    db.SaveChanges();
                    return RedirectToAction("UserProfile", "Tweets");
                }
            }
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult ShowTweets()
        {

            return PartialView();
        }

        public ActionResult FilterFeed()
        {
            var feedList = TempData["feedList"] as IEnumerable<Smutter.Models.follower>;

            var followFeeds = feedList.Select(fb => fb.followed_id);

            var filterFeeds = (from f in db.tweets.ToList()
                                where followFeeds.Contains(f.uid)
                                select f).ToList();

            TempData["FeedsList"] = filterFeeds;

            return RedirectToAction("GenerateFeeds", "Tweets");
        }

        public ActionResult GenerateFeeds()
        {

            var feedList = TempData["FeedsList"] as IEnumerable<Smutter.Models.tweet>;
            return View(feedList);
        }
        




    }
}
