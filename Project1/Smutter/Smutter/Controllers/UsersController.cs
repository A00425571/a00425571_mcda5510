﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Smutter.Models;

namespace Smutter.Controllers
{
    public class UsersController : Controller
    {
        private UserContext db = new UserContext();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Register()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "id,username,firstname,lastname,password,email,bio,dob")] user user)
        {
            if (ModelState.IsValid)
            {
                db.users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Login","Users");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,username,firstname,lastname,password,email,bio,dob")] user user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details","Users",new { id = user.id });
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            user user = db.users.Find(id);
            db.users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Login()
        {
            if (Session["uid"] != null)
            {
                return RedirectToAction("UserProfile", "Tweets");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult Login(user usr)
        {
            var postUser = db.users.Where(u => u.username == usr.username && u.password == usr.password).FirstOrDefault();
            if (postUser != null)
            {

                Session["username"] = postUser.username;
                Session["uid"] = postUser.id;
                return RedirectToAction("UserProfile", "Tweets");

            }
            else
            {
                ModelState.AddModelError("", "Username or Password is Incorrect");
            }

            return View();

        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login", "Users");
        }

        public ActionResult FollowList(IEnumerable<Smutter.Models.user> uList)
        {
            var notFollowedList = TempData["notFollowed"] as IEnumerable<Smutter.Models.user>;
            return View(notFollowedList);
        }

        public ActionResult FollowUser(int id)
        {

            return RedirectToAction("addToFollowList", "Follow", new { id = id});
        }

        public ActionResult FilterFollowList()
        {


            var list = TempData["userList"] as IEnumerable<Smutter.Models.follower>;
            var fullList = TempData["fullList"] as IEnumerable<Smutter.Models.follower>;

            // var rejectList = list.Where(i => list.Contains(i.followed_id));

            //var filteredList = fullList.Except(rejectList);

            IEnumerable<Smutter.Models.user> userList = from myRow in db.users.AsEnumerable()
                       where myRow.id.Equals(list)
                       select myRow;

            var query = list.Where(x => list
                    .Select(h => h.followed_id)
                    .Contains(x.followed_id));

            //var filterList = fullList.Where(f => !query.Contains(f.followed_id)).ToList();
           
            var followed = list.Select(fb => fb.followed_id);

            var notFollowedList = (from f in db.users.ToList()
                         where !followed.Contains(f.id)
                         select f).ToList();

            TempData["notFollowed"] = notFollowedList;

            return RedirectToAction("FollowList", "Users");

        }

        public ActionResult Following()
        {
            var followedList = TempData["followedList"] as IEnumerable<Smutter.Models.user>;
            return View(followedList);
        }

        public ActionResult FilterFollowingList()
        {
            var followingList = TempData["followList"] as IEnumerable<Smutter.Models.follower>;
            var followed = followingList.Select(fb => fb.followed_id);

            var FollowedList = (from f in db.users.ToList()
                                   where followed.Contains(f.id)
                                   select f).ToList();

            TempData["followedList"] = FollowedList;

            return RedirectToAction("Following", "Users");
        }


        public ActionResult UnFollowUser(int id)
        {

            return RedirectToAction("doUnFollow", "Follow", new { id = id });
        }

    }
}
