﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Smutter.Models;

namespace Smutter.Controllers
{
    public class FollowController : Controller
    {
        private FollowContext db = new FollowContext();

        // GET: Follow
        public ActionResult Index()
        {
            return View(db.followers.ToList());
        }

        // GET: Follow/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            follower follower = db.followers.Find(id);
            if (follower == null)
            {
                return HttpNotFound();
            }
            return View(follower);
        }

        // GET: Follow/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Follow/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "fid,follower_id,followed_id")] follower follower)
        {
            if (ModelState.IsValid)
            {
                db.followers.Add(follower);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(follower);
        }

        // GET: Follow/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            follower follower = db.followers.Find(id);
            if (follower == null)
            {
                return HttpNotFound();
            }
            return View(follower);
        }

        // POST: Follow/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "fid,follower_id,followed_id")] follower follower)
        {
            if (ModelState.IsValid)
            {
                db.Entry(follower).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(follower);
        }

        // GET: Follow/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            follower follower = db.followers.Find(id);
            if (follower == null)
            {
                return HttpNotFound();
            }
            return View(follower);
        }

        // POST: Follow/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            follower follower = db.followers.Find(id);
            db.followers.Remove(follower);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult addToFollowList(int id)
        {

            if (ModelState.IsValid)
            {
                follower f = new follower();
                f.follower_id = System.Convert.ToInt32(Session["uid"]);
                f.followed_id = id;
                db.followers.Add(f);
                db.SaveChanges();
                return RedirectToAction("getFollowedList", "Follow");
            }
            return View();
        }

        public ActionResult getFollowedList()
        {
            if(Session["uid"] != null)
            {
                var followed = from myRow in db.followers.AsEnumerable()
                               where myRow.follower_id == System.Convert.ToInt32(Session["uid"])
                               select myRow;

                IEnumerable<Smutter.Models.follower> list = followed.AsEnumerable().Select(
                    o => new follower
                    {
                        followed_id = o.followed_id
                    }

                    ).ToList();

                TempData["userList"] = list;
                TempData["fullList"] = db.followers.ToList();
                return RedirectToAction("FilterFollowList", "Users");
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }

        }

        public ActionResult getFollowingList()
        {

            if(Session["uid"]!= null)
            {
                var followed = from myRow in db.followers.AsEnumerable()
                               where myRow.follower_id == System.Convert.ToInt32(Session["uid"])
                               select myRow;

                IEnumerable<Smutter.Models.follower> list = followed.AsEnumerable().Select(
                    o => new follower
                    {
                        followed_id = o.followed_id
                    }

                    ).ToList();

                TempData["followList"] = list;

                return RedirectToAction("FilterFollowingList", "Users");
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }

        }

        public ActionResult GetFollowingFeed()
        {
            var followed = from myRow in db.followers.AsEnumerable()
                           where myRow.follower_id == System.Convert.ToInt32(Session["uid"])
                           select myRow;

            IEnumerable<Smutter.Models.follower> list = followed.AsEnumerable().Select(
                o => new follower
                {
                    followed_id = o.followed_id
                }

                ).ToList();


            TempData["feedList"] = list;

            return RedirectToAction("FilterFeed", "Tweets");
        }


        public ActionResult doUnFollow(int id)
        {
            int userId = System.Convert.ToInt32(Session["uid"]);
            int unfollowId = id;

            var lookupFollowers = db.followers.Where(u => u.follower_id == userId && u.followed_id == unfollowId).FirstOrDefault();

            if(lookupFollowers != null)
            {

                follower follower = db.followers.Find(lookupFollowers.fid);
                db.followers.Remove(follower);
                db.SaveChanges();
                return RedirectToAction("getFollowingList", "Follow");
            }

            return View();
        }
    }
}
