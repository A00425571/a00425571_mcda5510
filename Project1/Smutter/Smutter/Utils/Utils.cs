﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Smutter.Models;
using Smutter.Controllers;

namespace Smutter.Utils
{
    public static class Utils
    {
       
        public static string getUsername(int uid)
        {
            UserContext db = new UserContext();
            var postUser = db.users.Where(u => u.id == uid).FirstOrDefault();

            return postUser.username.ToString();
        }
    }
}